let navMonthIndex = 0;
let clicked = null;
let calendarEvents = localStorage.getItem('events')
  ? JSON.parse(localStorage.getItem('events'))
  : [];

const calendar = document.getElementById('calendar');
const newEventModal = document.getElementById('newEventModal');
const deleteEventModal = document.getElementById('deleteEventModal');
const backDrop = document.getElementById('modalBackDrop');
const eventTitleInput = document.getElementById('eventTitleInput');
const weekdays = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

function renderCalendar() {
  const dt = new Date();

  if (navMonthIndex !== 0) {
    dt.setMonth(new Date().getMonth() + navMonthIndex);
  }

  const day = dt.getDate();
  const month = dt.getMonth();
  const year = dt.getFullYear();

  const firstDayOfMonth = new Date(year, month, 1);
  const daysInMouth = new Date(year, month + 1, 0).getDate();
  const dateString = firstDayOfMonth.toLocaleDateString('en-us', {
    weekday: 'long',
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  });

  const paddingDays = weekdays.indexOf(dateString.split(', ')[0]);

  document.getElementById('monthDisplay').innerText = `${dt.toLocaleDateString(
    'en-us',
    { month: 'long' }
  )} ${year}`;

  calendar.innerHTML = '';

  for (let i = 1; i <= paddingDays + daysInMouth; i++) {
    const daySquare = document.createElement('div');
    daySquare.classList.add('day');

    const dayString = `${month + 1}/${i - paddingDays}/${year}`;

    if (i > paddingDays) {
      daySquare.innerText = i - paddingDays;

      const eventForDay = calendarEvents.find(e => e.date === dayString);

      if (eventForDay) {
        const eventDiv = document.createElement('div');
        eventDiv.classList.add('event');
        eventDiv.innerText = eventForDay.title;
        daySquare.appendChild(eventDiv);
      }

      if (i - paddingDays === day && navMonthIndex === 0) {
        daySquare.id = 'currentDay';
      }

      daySquare.addEventListener('click', () => openModal(dayString));
    } else {
      daySquare.classList.add('padding');
    }

    calendar.appendChild(daySquare);
  }
}

function openModal(date) {
  clicked = date;

  const eventForDay = calendarEvents.find(e => e.date === clicked);

  if (eventForDay) {
    document.getElementById('eventText').innerText = eventForDay.title;
    deleteEventModal.style.display = 'block';
  } else {
    newEventModal.style.display = 'block';
  }

  backDrop.style.display = 'block';
}

function closeModal() {
  eventTitleInput.classList.remove('error');
  deleteEventModal.style.display = 'none';
  newEventModal.style.display = 'none';
  backDrop.style.display = 'none';
  eventTitleInput.value = '';
  clicked = null;
  renderCalendar();
}

function saveCalendarEvent() {
  if (eventTitleInput.value) {
    eventTitleInput.classList.remove('error');

    calendarEvents.push({
      date: clicked,
      title: eventTitleInput.value,
    });

    localStorage.setItem('events', JSON.stringify(calendarEvents));

    closeModal();
  } else {
    eventTitleInput.classList.add('error');
  }
}

function deleteCalendarEvent() {
  calendarEvents = calendarEvents.filter(e => e.date !== clicked);
  localStorage.setItem('events', JSON.stringify(calendarEvents));
  closeModal();
}

function initButtons() {
  document.getElementById('nextButton').addEventListener('click', () => {
    navMonthIndex++;
    renderCalendar();
  });

  document.getElementById('backButton').addEventListener('click', () => {
    navMonthIndex--;
    renderCalendar();
  });
  document.getElementById('saveButton').addEventListener('click', saveCalendarEvent);
  document.getElementById('cancelButton').addEventListener('click', closeModal);
  document
    .getElementById('deleteButton')
    .addEventListener('click', deleteCalendarEvent);
  document.getElementById('closeButton').addEventListener('click', closeModal);
}

initButtons();
renderCalendar();
